.PHONY: all
all: clean fmt test

.PHONY: clean
clean:
	rm -rf target

.PHONY: fmt
fmt:
	cargo fmt

.PHONY: install
install: target/release/homeassistant-mqtt-rpi-sensehat
	sudo systemctl disable --now homeassistant-mqtt-rpi-sensehat.service || true
	sudo cp -av systemd/homeassistant-mqtt-rpi-sensehat.service /etc/systemd/system/
	sudo cp -av target/release/homeassistant-mqtt-rpi-sensehat /usr/local/bin
	sudo systemctl daemon-reload
	sudo systemctl enable --now homeassistant-mqtt-rpi-sensehat.service

target/release/homeassistant-mqtt-rpi-sensehat: Cargo.toml src/main.rs
	cargo build --all-features --all-targets --release --workspace

.PHONY: test
test:
	cargo fmt -- --check
	cargo clippy --all-features --all-targets --workspace
	RUST_BACKTRACE=1 cargo test --all-features --all-targets --workspace

.PHONY: uninstall
uninstall:
	sudo systemctl disable --now homeassistant-mqtt-rpi-sensehat.service
	sudo rm -fv /etc/systemd/system/homeassistant-mqtt-rpi-sensehat.service
	sudo rm -fv /usr/local/bin/homeassistant-mqtt-rpi-sensehat
	sudo systemctl daemon-reload

