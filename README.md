# homeassistant-mqtt-rpi-sensehat

## getting started

- run an MQTT broker somewhere e.g. mosquitto.org
- define a record for "mqtt" in your /etc/hosts pointing to the MQTT broker
- `make install`

## roadmap

- [x] `make install` and `make uninstall`
- [x] systemd unit/service: homeassistant-mqtt-rpi-sensehat.service
- [x] reads data from Raspberry Pi SenseHAT
- [x] connects to MQTT service hosted at mqtt:1883
- [x] use own hostname as `unique_id`
- [x] use each SenseHAT sensor as a separate `node_id`
- [x] use "homeassistant" as `discovery_prefix`
- [x] use "sensor" as `component`
- [x] sends `homeassistant/sensor/<unique_id>/config` on boot
- [x] sends `homeassistant/sensor/<unique_id>/state` on schedule
- [x] round numeric output to 3 decimal places
- [x] `MQTT_USER` and `MQTT_PASS` environment variable for MQTT authentication
- [x] `MQTT_HOST` and `MQTT_PORT` environment variable for MQTT connection

## configuration

- run `systemctl edit homeassistant-mqtt-rpi-sensehat.service` with something like:

  ```
  [Service]
  Environment=MQTT_HOST="..." MQTT_PORT="..." MQTT_USER="..." MQTT_PASS="..."
  ```

## see also

- https://www.raspberrypi.org/products/sense-hat/
- https://www.home-assistant.io/docs/mqtt/
- https://www.home-assistant.io/docs/mqtt/discovery/
- https://www.home-assistant.io/integrations/sensor.mqtt/
- https://developers.home-assistant.io/docs/device\_registry\_index

