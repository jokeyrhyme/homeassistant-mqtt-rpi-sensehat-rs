#![deny(clippy::all, unsafe_code)]

use std::{env, fs, path::Path, thread, time::Duration};

use std::sync::{Arc, Mutex};

use rumqttc::QoS;
use rumqttc::{Client, MqttOptions, Transport};
use sensehat::SenseHat;
use serde::Serialize;
use tracing::{event, Level};

fn main() {
    tracing_subscriber::fmt::init();
    event!(Level::INFO, "initialising");

    let ev = match envy::from_env::<EnvironmentVariables>() {
        Ok(ev) => ev,
        Err(e) => panic!("{:#?}", e),
    };
    event!(Level::INFO, "environment variables = {:?}", &ev);

    let try_mqtt_options = ev.try_into();

    let mut try_hat = SenseHat::new();
    match try_hat {
        Ok(ref mut hat) => {
            hat.clear().expect("unable to clear LED matrix");
        }
        Err(ref e) => {
            event!(Level::ERROR, "unable to initialise SenseHAT: {:?}", e);
        }
    }
    let me_ostr = hostname::get().expect("unable to determine hostname");
    let me = me_ostr.to_string_lossy().into_owned();
    event!(Level::INFO, "hostname = {}", &me);

    let serial_number = get_serial_number();
    event!(Level::INFO, "serial_number = {}", &serial_number);
    let serial_number_for_state = serial_number.clone();

    let suggested_area = get_suggested_area(&me);
    let device = Device::new(&me, &serial_number, &suggested_area);
    event!(Level::INFO, "device = {:?}", &device);

    if let (Ok(mqtt_options), Ok(mut hat)) = (try_mqtt_options, try_hat) {
        let (client, mut connection) = Client::new(mqtt_options, 10);
        event!(Level::INFO, "MQTT client = created");

        let client = Arc::new(Mutex::new(client));

        let client_arc = Arc::clone(&client);
        thread::spawn(move || loop {
            for sensor in [Sensor::Humidity, Sensor::Pressure, Sensor::Temperature] {
                publish_config(&device, &serial_number, &sensor, &client_arc);
            }
            thread::sleep(Duration::from_secs(97));
        });

        loop {
            match (
                hat.get_humidity(),
                hat.get_pressure(),
                hat.get_temperature_from_humidity(),
                hat.get_temperature_from_pressure(),
            ) {
                (Ok(_), Ok(_), Ok(_), Ok(_)) => break,
                _ => thread::sleep(Duration::from_secs(3)),
            }
        }
        event!(Level::INFO, "got SenseHAT values");

        let client_arc = Arc::clone(&client);
        thread::spawn(move || loop {
            if let (
                Ok(humidity),
                Ok(pressure),
                Ok(temperature_from_humidity),
                Ok(temperature_from_pressure),
            ) = (
                hat.get_humidity(),
                hat.get_pressure(),
                hat.get_temperature_from_humidity(),
                hat.get_temperature_from_pressure(),
            ) {
                let temperature = average(&[
                    temperature_from_humidity.as_celsius(),
                    temperature_from_pressure.as_celsius(),
                ]);
                for (sensor, value) in [
                    (Sensor::Humidity, humidity.as_percent()),
                    (Sensor::Pressure, pressure.as_hectopascals()),
                    (Sensor::Temperature, temperature),
                ] {
                    publish_state(&serial_number_for_state, &sensor, value, &client_arc);
                }
            }

            thread::sleep(Duration::from_secs(13));
        });

        for (i, notification) in connection.iter().enumerate() {
            if let Err(e) = notification {
                event!(Level::ERROR, "{} {:?}", i, e);
                thread::sleep(Duration::from_secs(3));
            }
        }
    }
}

const SERIAL_NUMBER_PATH: &str = "/sys/firmware/devicetree/base/serial-number";

enum Sensor {
    Humidity,
    Pressure,
    Temperature,
}
impl std::fmt::Display for Sensor {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Sensor::Humidity => write!(f, "HAT-humidity"),
            Sensor::Pressure => write!(f, "HAT-pressure"),
            Sensor::Temperature => write!(f, "HAT-temperature"),
        }
    }
}

#[derive(Clone, Debug, Serialize)]
struct Device {
    cns: Vec<[String; 2]>,
    ids: Vec<String>,
    name: String,
    mf: String,
    mdl: String,
    sw: String,
    sa: String,
}
impl Device {
    fn new(hostname: &str, serial_number: &str, suggested_area: &str) -> Self {
        Self {
            cns: vec![],
            ids: vec![String::from(serial_number)],
            name: String::from(hostname),
            mf: String::from("Raspberry Pi"),
            mdl: String::from("SenseHAT"),
            sw: String::from(env!("CARGO_PKG_VERSION")),
            sa: String::from(suggested_area),
        }
    }
}

#[derive(Debug, Serialize)]
struct Config {
    dev: Device,
    dev_cla: String,
    name: String,
    stat_cla: String,
    stat_t: String,
    uniq_id: String,
    unit_of_meas: String,
}
impl Config {
    fn new(device: &Device, serial_number: &str, sensor: &Sensor) -> Self {
        let state_topic = get_state_topic(serial_number, sensor);
        let uniq_id = get_unique_id(serial_number, sensor);

        let name = [device.sa.clone(), format!("{}", &sensor)].join(" ");

        match sensor {
            Sensor::Humidity => Self {
                dev: device.clone(),
                dev_cla: String::from("humidity"),
                name,
                stat_cla: String::from("measurement"),
                stat_t: state_topic,
                unit_of_meas: String::from("%"),
                uniq_id,
            },
            Sensor::Pressure => Self {
                dev: device.clone(),
                dev_cla: String::from("pressure"),
                name,
                stat_cla: String::from("measurement"),
                stat_t: state_topic,
                unit_of_meas: String::from("hPa"),
                uniq_id,
            },
            Sensor::Temperature => Self {
                dev: device.clone(),
                dev_cla: String::from("temperature"),
                name,
                stat_cla: String::from("measurement"),
                stat_t: state_topic,
                unit_of_meas: String::from("°C"),
                uniq_id,
            },
        }
    }
}

#[derive(Debug, serde::Deserialize)]
#[serde(default)]
struct EnvironmentVariables {
    mqtt_host: String,
    mqtt_pass: String,
    mqtt_port: u16,
    mqtt_user: String,
}
impl Default for EnvironmentVariables {
    fn default() -> Self {
        Self {
            mqtt_host: String::new(),
            mqtt_pass: String::new(),
            mqtt_port: 1883,
            mqtt_user: String::new(),
        }
    }
}
impl TryInto<MqttOptions> for EnvironmentVariables {
    type Error = Error;

    fn try_into(self) -> Result<MqttOptions, Error> {
        if self.mqtt_host.is_empty() {
            return Err(Error::NoMqttHost);
        }
        let me_ostr = hostname::get().expect("unable to determine hostname");
        let me = me_ostr.to_string_lossy().into_owned();
        let mut mqttoptions = MqttOptions::new(
            format!("{}@{}", env!("CARGO_PKG_NAME"), &me),
            &self.mqtt_host,
            self.mqtt_port,
        );
        mqttoptions.set_keep_alive(5);
        mqttoptions.set_transport(Transport::Tcp);
        if !self.mqtt_pass.is_empty() && !self.mqtt_user.is_empty() {
            event!(Level::INFO, "MQTT_USER = {}", self.mqtt_user);
            mqttoptions.set_credentials(&self.mqtt_user, &self.mqtt_pass);
        }
        Ok(mqttoptions)
    }
}

#[derive(Debug)]
enum Error {
    NoMqttHost,
}

fn average<V>(values: &V) -> f64
where
    V: Copy + std::iter::IntoIterator<Item = f64>,
{
    let (sum, count) = values
        .into_iter()
        .fold((0.0, 0), |accum, value| (accum.0 + value, accum.1 + 1));
    sum / (count as f64)
}

fn get_config_topic(serial_number: &str, sensor: &Sensor) -> String {
    format!(
        "homeassistant/sensor/{}/config",
        get_unique_id(serial_number, sensor)
    )
}

fn get_serial_number() -> String {
    match fs::read_to_string(Path::new(SERIAL_NUMBER_PATH)) {
        Ok(serial_number) => String::from(serial_number.trim().trim_end_matches('\u{0}')),
        // TODO: support serial numbers from other devices
        Err(_) => String::from("unknown"),
    }
}

fn get_state_topic(serial_number: &str, sensor: &Sensor) -> String {
    format!(
        "homeassistant/sensor/{}/state",
        get_unique_id(serial_number, sensor)
    )
}

fn get_suggested_area(hostname: &str) -> String {
    match hostname {
        "pi-bedroom1" => String::from("Bedroom1"),
        "pi-livingroom" => String::from("Living Room"),
        "pi-study" => String::from("Study"),
        _ => String::from(""),
    }
}

fn get_unique_id(serial_number: &str, sensor: &Sensor) -> String {
    format!("{}-{}", serial_number, sensor)
}

fn publish_config(
    device: &Device,
    serial_number: &str,
    sensor: &Sensor,
    client: &Arc<Mutex<Client>>,
) {
    let config = Config::new(device, serial_number, sensor);
    let topic = get_config_topic(serial_number, sensor);
    let payload = serde_json::to_vec(&config).expect("unable to serialise payload");
    let mut client = client.lock().unwrap();
    client
        .publish(&topic, QoS::AtLeastOnce, false, payload)
        .unwrap();
    event!(Level::INFO, "{} {:?}", &topic, &config);
}

fn publish_state(serial_number: &str, sensor: &Sensor, state: f64, client: &Arc<Mutex<Client>>) {
    event!(Level::INFO, "{} = {}", &sensor, &state);
    let mut client = client.lock().unwrap();
    client
        .publish(
            &get_state_topic(serial_number, sensor),
            QoS::AtLeastOnce,
            false,
            format!("{:.3}", state),
        )
        .unwrap();
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn average_of_two_floats() {
        let got = average(&[10.0, 20.0]);
        // shouldn't test equality with floats
        assert!(got < 15.01);
        assert!(14.99 < got);
    }
}
