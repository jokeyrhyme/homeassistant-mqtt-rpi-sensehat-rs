ARG PLATFORM

####
FROM rust:alpine AS build
ARG CARGO_BUILD_TARGET
ENV CARGO_BUILD_TARGET=$CARGO_BUILD_TARGET
ENV RUST_BACKTRACE=1

COPY . /repo
WORKDIR /repo

RUN apk add clang llvm musl-dev
# have to use LLVM because building with GCC is broken,
# see: https://github.com/briansmith/ring/issues/1414#issuecomment-1055177218
ENV CC_aarch64_unknown_linux_musl=clang
ENV AR_aarch64_unknown_linux_musl=llvm-ar
ENV CARGO_TARGET_AARCH64_UNKNOWN_LINUX_MUSL_RUSTFLAGS="-Clink-self-contained=yes -Clinker=rust-lld"

RUN rustup target add $CARGO_BUILD_TARGET
RUN cargo build --release --target $CARGO_BUILD_TARGET --workspace

####
FROM --platform=$PLATFORM alpine:latest
ARG CARGO_BUILD_TARGET

COPY --from=build /repo/target/$CARGO_BUILD_TARGET/release/homeassistant-mqtt-rpi-sensehat /app

ENV MQTT_HOST=""
ENV MQTT_PASS=""
ENV MQTT_PORT=1883
ENV MQTT_USER=""
ENTRYPOINT [ "/app" ]
